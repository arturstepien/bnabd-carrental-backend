package com.BnaBD.carrental.controller;

import javax.validation.Valid;

import com.BnaBD.carrental.dto.request.LoginRequest;
import com.BnaBD.carrental.dto.request.SignupRequest;
import com.BnaBD.carrental.dto.response.JwtResponse;
import com.BnaBD.carrental.repository.RoleRepository;
import com.BnaBD.carrental.repository.UserRepository;
import com.BnaBD.carrental.service.Auth.AuthService;
import com.BnaBD.carrental.service.Registration.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/auth")
public class AuthenticationController {
    private final AuthService authService;
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private RegistrationService registrationService;

    @Autowired
    public AuthenticationController(AuthService authService, UserRepository userRepository, RoleRepository roleRepository, RegistrationService registrationService) {
        this.authService = authService;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.registrationService = registrationService;
    }

    @PostMapping("/signin")
    public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        return  ResponseEntity.ok(authService.jwtResponse(loginRequest));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        return registrationService.saveUser(signUpRequest);
    }

}