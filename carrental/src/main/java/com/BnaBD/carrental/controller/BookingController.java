package com.BnaBD.carrental.controller;

import com.BnaBD.carrental.domain.Booking;
import com.BnaBD.carrental.dto.request.BookingRequest;
import com.BnaBD.carrental.dto.response.ChartResponse;
import com.BnaBD.carrental.exception.BookingUnavailableVehicleException;
import com.BnaBD.carrental.service.Booking.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class BookingController {

    private BookingService bookingService;

    @Autowired
    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @PostMapping("/addBooking")
    public ResponseEntity<Map<String, Object>> addBooking(@RequestBody BookingRequest bookingRequest) {
            return bookingService.addBooking(bookingRequest);
    }

    @GetMapping("/getBookings")
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    public ResponseEntity<List<Booking>> getBookings(){
        return bookingService.getBookings();
    }

    @GetMapping("/getMyBookings/{id}")
    public ResponseEntity<List<Booking>> getMyBookings(@PathVariable Long id){
        return bookingService.getMyBookings(id);
    }

    @GetMapping("/getMyRentedBookings/{id}")
    public ResponseEntity<List<Booking>> getMyRentedBookings(@PathVariable Long id){
        return bookingService.getMyRentedBookings(id);
    }

    @GetMapping("/getMyReservedBookings/{id}")
    public ResponseEntity<List<Booking>> getMyReservedBookings(@PathVariable Long id){
        return bookingService.getMyReservedBookings(id);
    }

    @GetMapping("/getRentedBookings")
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    public ResponseEntity<List<Booking>> getRentedBookings(){
        return bookingService.getRentedBookings();
    }

    @GetMapping("/getReservedBookings")
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    public ResponseEntity<List<Booking>> getReservedBookings(){
        return bookingService.getReservedBookings();
    }

    @GetMapping("/excelfile")
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    public void getAllBookingsInExcelFormat(HttpServletResponse response) {
        response.setHeader("Content-Encoding", "UTF-8");
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        List<Booking> bookingList = bookingService.getBookings().getBody();
        File xls = null;
        try {
            xls = bookingService.createExcelBookingListExelFile(bookingList);
            final FileInputStream in = new FileInputStream(xls);
            final OutputStream out = response.getOutputStream();

            final byte[] buffer = new byte[8192];
            int length;

            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }
            in.close();
            out.close();

        } catch (IOException e) {
            System.err.println("Cannot create booking list file in exel format: " + e.getMessage());
        }
    }

    @PutMapping("/rent/{id}")
    public void bookingRent(@PathVariable Long id) {
        bookingService.bookingRent(id);
    }

    @PutMapping("/return/{id}")
    public void bookingReturn(@PathVariable Long id) {
        bookingService.bookingReturn(id);
    }

    @PutMapping("/cancel/{id}")
    public void bookingCancel(@PathVariable Long id) {
        bookingService.bookingCancel(id);
    }

    @GetMapping("/getChartData")
    public List<ChartResponse> getChartData() {
        return bookingService.getChartData();
    }

}
