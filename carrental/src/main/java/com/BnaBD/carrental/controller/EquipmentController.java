package com.BnaBD.carrental.controller;

import com.BnaBD.carrental.domain.Equipment;
import com.BnaBD.carrental.domain.Vehicle;
import com.BnaBD.carrental.service.Equipment.EquipmentService;
import com.BnaBD.carrental.service.Vehicle.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/equipment")
public class EquipmentController {
    private EquipmentService equipmentService;

    @Autowired
    public EquipmentController(EquipmentService equipmentService) {
        this.equipmentService = equipmentService;
    }

    @GetMapping("/load")
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    public List<Equipment> getEquipment() {
        return equipmentService.getEquipment();
    }
    @RequestMapping(value = { "/delete/{eqpCode}" }, method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    public void deleteEquipment(@PathVariable String eqpCode) {
        equipmentService.deleteEquipmentById(eqpCode);
    }
    @RequestMapping(value = {"/add"}, method = RequestMethod.POST)
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    public void addEquipment(@RequestBody Equipment equipment) {
        equipmentService.addEquipment(equipment);
    }
}
