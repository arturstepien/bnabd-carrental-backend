package com.BnaBD.carrental.controller;

import com.BnaBD.carrental.domain.User;
import com.BnaBD.carrental.service.User.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
public class UserController {
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/getUserListForPage", method = RequestMethod.GET)
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    public Page<User> getUserListForPage(@RequestParam(value = "page") int page, @RequestParam(value = "number") int number) {
        return userService.getUsersForPage(PageRequest.of(page, number));
    }
    @RequestMapping(value="/getUserByID/{id}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    public User getUserByID(@PathVariable Long id) {
        return userService.getUserByID(id);
    }

    @RequestMapping(value="/updateUserData", method = RequestMethod.PUT)
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    public ResponseEntity<?> updateUserData(@RequestParam String username, @RequestParam String email, @RequestBody User data) {
        return userService.updateUserData(username, email, data);
    }
}

