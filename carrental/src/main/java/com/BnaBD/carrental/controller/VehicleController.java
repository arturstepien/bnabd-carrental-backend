package com.BnaBD.carrental.controller;

import com.BnaBD.carrental.domain.Vehicle;
import com.BnaBD.carrental.dto.request.VehicleRequest;
import com.BnaBD.carrental.service.Vehicle.VehicleService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api")
public class VehicleController {

    @Value("${carrental.app.imagesPath}")
    private String pathToImages;

    private VehicleService vehicleService;

    @Autowired
    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @GetMapping("/vehicles")
    public List<Vehicle> getVehicles() {
        return vehicleService.getVehicles();
    }


    @RequestMapping(value = "/vehicles/carList",method = RequestMethod.GET, params = { "page", "number" })
    public Page<Vehicle> getVehicleList(@RequestParam(value = "page") int page,
                                        @RequestParam(value = "number") int number) {
        return vehicleService.getVehiclesForPage(PageRequest.of(page, number));
    }


    @RequestMapping(value = "/vehicles/getVehiclesForPage", method = RequestMethod.GET)
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    public Page<Vehicle> getVehiclesForPage(@RequestParam(value = "page") int page, @RequestParam(value = "number") int number) {
        return vehicleService.getVehiclesForPage(PageRequest.of(page, number));
    }


    @RequestMapping(value="/vehicles/carDetails/{id}", method = RequestMethod.GET)
    public Vehicle getVehicleByIDNonAuthorized(@PathVariable String id) {
        return vehicleService.getVehicleByID(id);
    }

    @RequestMapping(value="/vehicles/getVehicleByID/{id}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    public Vehicle getVehicleByID(@PathVariable Long id) {
        return vehicleService.getVehicleByID(id);
    }

    @RequestMapping(value = "/vehicles/updateVehicle/{id}", method = RequestMethod.POST)
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    public Vehicle updateVehicle(@PathVariable Long id, VehicleRequest vehicleRequest,
                              @RequestParam(value = "image", required = false) MultipartFile image) {

        System.out.println("Updating vehicle.");

        if (image != null) {
            System.out.println("XD"+image);
            String photoName = updateVehicleImage(image, id);
            vehicleRequest.setFileName(photoName);
        }else {
            vehicleRequest.setFileName(vehicleService.getVehicleByID(id).getVehicleParameters().getPhotoName());
        }

        return vehicleService.updateVehicle(vehicleRequest);

    }
    @RequestMapping(value = "/vehicles/addCar", method = RequestMethod.POST)
    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    public Vehicle addCar(VehicleRequest vehicleRequest,
                                 @RequestParam(value = "image", required = true) MultipartFile image) {


        System.out.println("Adding vehicle to database.");

        String fileName = addVehicleImage(image);
        vehicleRequest.setFileName(fileName);

        return vehicleService.addVehicle(vehicleRequest);


    }
    private String addVehicleImage(MultipartFile image) {

        UUID uuid = UUID.randomUUID();

        String fileName = uuid.toString() + "." + FilenameUtils.getExtension(image.getOriginalFilename());

        try {
            byte[] bytes = image.getBytes();

            String filePath = pathToImages + "/" + fileName;

            System.out.println("Dodaje plik -> " + filePath);

            File fnew = new File(filePath);
            fnew.createNewFile();
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fnew));
            stream.write(bytes);
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileName;
    }

    private String updateVehicleImage(MultipartFile image, Long id) {
        Vehicle vehicle = vehicleService.getVehicleByID(id);
        String fileName = vehicle.getVehicleParameters().getPhotoName();
        try {
            byte[] bytes = image.getBytes();

            String filePath = pathToImages +"/"+fileName;
            System.out.println("Dodaje plik -> " + filePath);

            File fnew = new File(filePath);

            fnew.createNewFile();
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(fnew));
            stream.write(bytes);
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileName;

    }

}
