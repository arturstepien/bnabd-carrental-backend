package com.BnaBD.carrental.controller;

import com.BnaBD.carrental.domain.Vehicle;
import com.BnaBD.carrental.dto.request.VehicleFilterRequest;
import com.BnaBD.carrental.service.Vehicle.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = { "api/carlistsearch" })
public class VehicleFilterController {
    @Autowired
    VehicleService vehicleService;

    @RequestMapping(value = "/brandlist", method = RequestMethod.GET)
    public List<String> vehicleSearchGetBrandList() {

        return vehicleService.getBrandList();
    }

    @RequestMapping(value = "/bodytypelist", method = RequestMethod.GET)
    public List<String> vehicleSearchGetBodTypeList() {

        return vehicleService.getBodyTypeList();
    }

    @RequestMapping(value = "/colorlist", method = RequestMethod.GET)
    public List<String> vehicleSearchGetColorList() {

        return vehicleService.getColorList();
    }

    @RequestMapping(value = "/modelsforbrand", method = RequestMethod.POST)
    public List<String> getModelsForBrand(@RequestBody String brand) {
        return vehicleService.getModelListForBrand(brand);
    }
    @RequestMapping(value = "/carlist",method = RequestMethod.POST, params = { "page", "number" })
    public Page<Vehicle> getFiltredCarListForFirstPage(@RequestBody VehicleFilterRequest carFilter,
                                                       @RequestParam(value = "page") int page, @RequestParam(value = "number") int number) {

        return vehicleService.getFilteredCarListForPage(carFilter, PageRequest.of(page, number));
    }


}
