package com.BnaBD.carrental.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;


import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "bookings")
public class Booking {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "ID")
        private Long id;

        @JsonManagedReference
        @OneToOne(cascade = CascadeType.ALL)
        @JoinColumn(name = "vehicle_ID", referencedColumnName = "id")
        private Vehicle vehicle;


        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        @Column(name = "rentalDate")
        private LocalDateTime rentalDate;

        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        @Column(name = "returnDate")
        private LocalDateTime returnDate;

        @Column(name = "bookingStateCode")
        private String bookingStateCode;

        @Column(name = "totalCost")
        private BigDecimal totalCost;

        @ManyToOne
        @JoinColumn( name="user_ID", nullable = false)
        private User user;

}
