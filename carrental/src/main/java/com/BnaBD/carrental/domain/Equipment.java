package com.BnaBD.carrental.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@ToString
@Entity
@Table(name = "Equipment")
public class Equipment {

    @Id
    @Column
    @Size(max = 3)
    private String equipmentCode;

    @Column
    private String description;

    @ManyToMany
    private List<Vehicle> carList = new ArrayList<>();


}


