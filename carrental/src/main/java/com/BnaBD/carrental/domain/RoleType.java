package com.BnaBD.carrental.domain;

public enum RoleType {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_EMPLOYEE

}
