package com.BnaBD.carrental.domain;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "vehicles")
public class Vehicle implements Serializable {
    @Id
    private Long id;

    @Column(name = "registration")
    private String registration;

    @Column(name = "brand")
    private String brand;

    @Column(name = "model")
    private String model;

    @Column(name = "dailyFee")
    private BigDecimal dailyFee;

    @Column(name = "vehicleStatus")
    private String vehicleStatus;

    @Column(name = "bestOffer")
    private Boolean bestOffer;

    @JsonBackReference
    @OneToOne(mappedBy = "vehicle")
    private Booking booking;


    @OneToOne(mappedBy = "vehicle")
    private VehicleParameters vehicleParameters;

    @ManyToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH })
    @JoinTable(name = "vehicle_equipment", joinColumns = @JoinColumn(name = "vehicle_id"), inverseJoinColumns = @JoinColumn(name = "equipment_id"))
    private List<Equipment> equipmentList = new ArrayList<>();

    public Vehicle(String registration, String brand, String model, BigDecimal dailyFee, Long locationId, String vehicleStatus) {
        this.registration = registration;
        this.brand = brand;
        this.model = model;
        this.dailyFee = dailyFee;
        this.vehicleStatus = vehicleStatus;
    }
}
