package com.BnaBD.carrental.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "Vehicle_Parameters")
public class VehicleParameters{

    @Id
    @JsonIgnore
    @Column(name = "vehicle_id")
    private Long vehicleID;

    @Column(name = "bodytype")
    private String bodytype;

    @Column(name = "productionYear")
    private Integer productionYear;

    @Column(name = "fuelType")
    private String fuelType;

    @Column(name = "power")
    private Integer power;

    @Column(name = "gearbox")
    private String gearbox;

    @Column(name = "frontWheelDrive")
    private Boolean frontWheelDrive;

    @Column(name = "doorsNumber")
    private Integer doorsNumber;

    @Column(name = "seatsNumber")
    private Integer seatsNumber;

    @Column(name = "color")
    private String color;

    @Column(name = "metallic")
    private Boolean metallic;

    @Column(name = "photoName")
    private String photoName;

    @Column(name = "description")
    private String description;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;

    public VehicleParameters(Long vehicleID, String bodytype, Integer productionYear, String fuelType, Integer power,
                             String gearbox, Boolean frontWheelDrive, Integer doorsNumber, Integer seatsNumber, String color,
                             Boolean metallic, String photoName, String description) {
        this.vehicleID = vehicleID;
        this.bodytype = bodytype;
        this.productionYear = productionYear;
        this.fuelType = fuelType;
        this.power = power;
        this.gearbox = gearbox;
        this.frontWheelDrive = frontWheelDrive;
        this.doorsNumber = doorsNumber;
        this.seatsNumber = seatsNumber;
        this.color = color;
        this.metallic = metallic;
        this.photoName = photoName;
        this.description = description;
    }
}
