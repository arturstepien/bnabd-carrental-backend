package com.BnaBD.carrental.dto.request;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class VehicleFilterRequest {
        private String brand;
        private String model;
        private String bodytype;
        private BigDecimal priceFrom;
        private BigDecimal priceTo;
        private Integer placesNumberFrom;
        private Integer placesNumberTo;
        private Integer doorsNumberFrom;
        private Integer doorsNumberTo;
        private Integer productionYearFrom;
        private Integer productionYearTo;
        private String color;
}

