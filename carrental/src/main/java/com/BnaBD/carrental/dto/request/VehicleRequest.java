package com.BnaBD.carrental.dto.request;

import lombok.*;

import java.math.BigDecimal;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class VehicleRequest {
    private Long id;
    private String brand;
    private String model;
    private BigDecimal dailyFee;
    private String registration;
    private String vehicleStatus;
    private Boolean bestOffer;

    private String bodytype;
    private String fuelType;
    private Integer power;
    private String gearbox;
    private Boolean frontWheelDrive;
    private Integer doorsNumber;
    private Integer seatsNumber;
    private String color;
    private Boolean metallic;
    private String description;
    private String fileName;
    private Integer productionYear;

}
