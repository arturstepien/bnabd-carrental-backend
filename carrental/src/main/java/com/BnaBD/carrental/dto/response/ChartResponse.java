package com.BnaBD.carrental.dto.response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChartResponse {

    private Long value;
    private String month;

}
