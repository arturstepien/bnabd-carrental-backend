package com.BnaBD.carrental.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VehicleResponse {
    private String registration;
    private String brand;
    private String model;
    private Double dailyFee;
    private String vehicleStatus;

}
