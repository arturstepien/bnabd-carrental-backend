package com.BnaBD.carrental.repository;

import com.BnaBD.carrental.domain.Booking;
import com.BnaBD.carrental.dto.response.ChartResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BookingRepository extends JpaRepository<Booking, Long> {


    @Query("SELECT b FROM Booking b WHERE b.bookingStateCode='REN'")
    List<Booking> getRentedBookings();

    @Query("SELECT b FROM Booking b WHERE b.bookingStateCode='RES'")
    List<Booking> getReservedBookings();

    @Query("SELECT b FROM Booking b WHERE b.user.id=:id ORDER BY b.id")
    List<Booking> getMyBookings(Long id);

    @Query("SELECT b FROM Booking b WHERE b.user.id=:id AND b.bookingStateCode='RES' ORDER BY b.id")
    List<Booking> getMyReservedBookings(Long id);

    @Query("SELECT b FROM Booking b WHERE b.user.id=:id AND b.bookingStateCode='REN' ORDER BY b.id")
    List<Booking> getMyRentedBookings(Long id);

    @Query(value = "SELECT new com.BnaBD.carrental.dto.response.ChartResponse( COUNT(b.id),to_char(b.rentalDate, 'Month') ) FROM Booking b GROUP BY to_char(b.rentalDate, 'Month')  ORDER BY to_char(b.rentalDate, 'Month')")
    List<ChartResponse> getChartData();
}
