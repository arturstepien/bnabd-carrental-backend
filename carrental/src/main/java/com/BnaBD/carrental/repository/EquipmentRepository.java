package com.BnaBD.carrental.repository;

import com.BnaBD.carrental.domain.Equipment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EquipmentRepository extends JpaRepository<Equipment, Long> {
    @Query("SELECT e FROM Equipment e WHERE e.equipmentCode=:equipmentCode")
    Equipment getEquipmentByCode(@Param("equipmentCode") String equipmentCode);
}

