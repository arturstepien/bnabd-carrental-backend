package com.BnaBD.carrental.repository;



import com.BnaBD.carrental.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    @Query(value = "SELECT DISTINCT u FROM User u", countQuery = "SELECT COUNT(u) FROM User u")
    Page<User> getUsersForPage(Pageable pageable);
//
//    @Query("SELECT u.name, u.surname from User u  WHERE u.login = :userId")
//    String getDataUser(@Param("userId") String userId);
//
//    @Transactional
//    @Modifying
//    @Query("UPDATE User u SET u.password = crypt(:userPassword, gen_salt('bf')), u.shouldChangePassword = false WHERE u.login = :userId")
//    void setUserPasswordById(@Param("userId") String userId, @Param("userPassword") String userPassword);
}
