package com.BnaBD.carrental.repository;


import com.BnaBD.carrental.domain.VehicleParameters;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehicleParametersRepository extends JpaRepository<VehicleParameters, Long> {
}
