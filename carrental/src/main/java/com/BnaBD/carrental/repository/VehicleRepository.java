package com.BnaBD.carrental.repository;

import com.BnaBD.carrental.domain.User;
import com.BnaBD.carrental.domain.Vehicle;
import com.BnaBD.carrental.dto.request.VehicleFilterRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
    @Override
    List<Vehicle> findAll();

    @Query(value = "SELECT DISTINCT v FROM Vehicle v ORDER BY v.id", countQuery = "SELECT COUNT(v) FROM Vehicle v")
    Page<Vehicle> getVehiclesForPage(Pageable pageable);

    @Query("SELECT distinct v.brand FROM Vehicle v")
    List<String> getBrandList();

    @Query("SELECT distinct vp.bodytype FROM Vehicle v JOIN VehicleParameters vp ON (v.id=vp.vehicleID)")
    List<String> getBodyTypeList();

    @Query("SELECT distinct v.model FROM Vehicle v WHERE v.brand=:br")
    List<String> getModelListForBrand(String br);

    @Query("SELECT distinct vp.color FROM Vehicle v JOIN VehicleParameters vp ON (v.id=vp.vehicleID)")
    List<String> getColorList();

    @Query(value = "SELECT v FROM Vehicle v JOIN VehicleParameters vp ON (v.id=vp.vehicleID) WHERE "
            + "(:brand IS NULL OR v.brand=:brand) AND " + "(:model IS NULL OR v.model=:model) AND " + "(:bodytype IS NULL OR vp.bodytype=:bodytype) AND "
            + "((:priceFrom IS NULL OR v.dailyFee > :priceFrom) AND (:priceTo IS NULL OR v.dailyFee < :priceTo)) AND "
            + "((:placesNumberFrom IS NULL OR vp.seatsNumber > :placesNumberFrom) AND (:placesNumberTo IS NULL OR vp.seatsNumber < :placesNumberTo)) AND "
            + "((:doorsNumberFrom IS NULL OR vp.doorsNumber > :doorsNumberFrom) AND (:doorsNumberTo IS NULL OR vp.doorsNumber < :doorsNumberTo)) AND "
            + "((:productionYearFrom IS NULL OR vp.productionYear > :productionYearFrom) AND (:productionYearTo IS NULL OR vp.productionYear < :productionYearTo)) AND "
            + "(:color IS NULL OR vp.color=:color) " + " ORDER BY v.id ",
            countQuery = "SELECT COUNT(v) FROM Vehicle v JOIN VehicleParameters vp ON (v.id=vp.vehicleID) WHERE "
                    + "(:brand IS NULL OR v.brand=:brand) AND " + "(:model IS NULL OR v.model=:model) AND " + "(:bodytype IS NULL OR vp.bodytype=:bodytype) AND "
                    + "((:priceFrom IS NULL OR v.dailyFee > :priceFrom) AND (:priceTo IS NULL OR v.dailyFee < :priceTo)) AND "
                    + "((:placesNumberFrom IS NULL OR vp.seatsNumber > :placesNumberFrom) AND (:placesNumberTo IS NULL OR vp.seatsNumber < :placesNumberTo)) AND "
                    + "((:doorsNumberFrom IS NULL OR vp.doorsNumber > :doorsNumberFrom) AND (:doorsNumberTo IS NULL OR vp.doorsNumber < :doorsNumberTo)) AND "
                    + "((:productionYearFrom IS NULL OR vp.productionYear > :productionYearFrom) AND (:productionYearTo IS NULL OR vp.productionYear < :productionYearTo)) AND "
                    + "(:color IS NULL OR vp.color=:color) " + " ORDER BY v.id DESC ")
    Page<Vehicle> getFilteredCarListForPage(String brand,
                                            String model,
                                            String bodytype,
                                            BigDecimal priceFrom,
                                            BigDecimal priceTo,
                                            Integer placesNumberFrom,
                                            Integer placesNumberTo,
                                            Integer doorsNumberFrom,
                                            Integer doorsNumberTo,
                                            Integer productionYearFrom,
                                            Integer productionYearTo,
                                            String color,
                                            Pageable pageable);

}
