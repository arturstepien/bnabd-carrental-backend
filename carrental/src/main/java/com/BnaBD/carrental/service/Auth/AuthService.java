package com.BnaBD.carrental.service.Auth;

import com.BnaBD.carrental.dto.request.LoginRequest;
import com.BnaBD.carrental.dto.response.JwtResponse;
import com.BnaBD.carrental.security.JwtUtils;
import com.BnaBD.carrental.service.UserDetails.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthService {

    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;

    @Autowired
    public AuthService(AuthenticationManager authenticationManager, JwtUtils jwtUtils) {
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
    }
    public JwtResponse jwtResponse(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        return new JwtResponse(generateJwtToken(authentication), userDetails.getId(), userDetails.getUsername(),userDetails.getEmail(), roles(userDetails), userDetails.getFirstName(), userDetails.getSecondName());
    }

    private String generateJwtToken(Authentication authentication){
        return jwtUtils.generateJwtToken(authentication);
    }

    private List<String> roles(UserDetailsImpl userDetails){
        return userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
    }
}
