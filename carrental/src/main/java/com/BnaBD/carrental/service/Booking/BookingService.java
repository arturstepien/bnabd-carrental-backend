package com.BnaBD.carrental.service.Booking;

import com.BnaBD.carrental.domain.Booking;
import com.BnaBD.carrental.dto.request.BookingRequest;
import com.BnaBD.carrental.dto.response.ChartResponse;
import com.BnaBD.carrental.exception.BookingUnavailableVehicleException;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface BookingService {
    ResponseEntity<Map<String, Object>> addBooking(BookingRequest bookingRequest);
    ResponseEntity<List<Booking>> getBookings();
    ResponseEntity<List<Booking>> getRentedBookings();
    ResponseEntity<List<Booking>> getReservedBookings();
    ResponseEntity<List<Booking>> getMyBookings(Long id);
    ResponseEntity<List<Booking>> getMyReservedBookings(Long id);
    ResponseEntity<List<Booking>> getMyRentedBookings(Long id);
    File createExcelBookingListExelFile(List<Booking> bookingList) throws IOException;
    void bookingRent(Long id);
    void bookingReturn(Long id);
    void bookingCancel(Long id);
    List<ChartResponse> getChartData();
}
