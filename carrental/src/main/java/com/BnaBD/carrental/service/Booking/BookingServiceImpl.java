package com.BnaBD.carrental.service.Booking;

import com.BnaBD.carrental.domain.Booking;
import com.BnaBD.carrental.domain.User;
import com.BnaBD.carrental.domain.Vehicle;
import com.BnaBD.carrental.dto.request.BookingRequest;
import com.BnaBD.carrental.dto.response.ChartResponse;
import com.BnaBD.carrental.exception.BookingUnavailableVehicleException;
import com.BnaBD.carrental.exception.RecordSaveException;
import com.BnaBD.carrental.repository.BookingRepository;
import com.BnaBD.carrental.repository.UserRepository;
import com.BnaBD.carrental.repository.VehicleRepository;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BookingServiceImpl implements BookingService {

    private VehicleRepository vehicleRepository;
    private UserRepository userRepository;
    private BookingRepository bookingRepository;

    @Autowired
    public BookingServiceImpl(VehicleRepository vehicleRepository, UserRepository userRepository, BookingRepository bookingRepository) {
        this.vehicleRepository = vehicleRepository;
        this.userRepository = userRepository;
        this.bookingRepository = bookingRepository;
    }

    @Override
    public ResponseEntity<Map<String, Object>> addBooking(BookingRequest bookingRequest){
        Vehicle vehicle = vehicleRepository.findById(bookingRequest.getVehicleID()).get();
        User user = userRepository.findById(bookingRequest.getUserID()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("vehicle", vehicle);
        response.put("user", user);
        String error = null;
        if (vehicle != null && user != null) {
            if (vehicle.getVehicleStatus().equals("UAV")) {
                response.put("error", new BookingUnavailableVehicleException("Cannot book unavailable vehicle."));
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            } else {
                Booking booking = Booking.builder()
                        .rentalDate(bookingRequest.getRentalDate())
                        .returnDate(bookingRequest.getReturnDate())
                        .bookingStateCode(bookingRequest.getBookingStateCode())
                        .totalCost(bookingRequest.getTotalCost())
                        .user(user)
                        .vehicle(vehicle).build();
                vehicle.setVehicleStatus("UAV");
                response.put("booking", booking);
                try {
                    vehicleRepository.save(vehicle);
                    bookingRepository.save(booking);
                } catch (Exception e) {
                    response.put("error", new RecordSaveException("Cannot save record to database"));
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
                }
            }
        }
        response.put("message", "Booking successfully created");
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @Override
    public ResponseEntity<List<Booking>> getBookings() {
        return ResponseEntity.ok(bookingRepository.findAll());
    }

    @Override
    public ResponseEntity<List<Booking>> getRentedBookings() {
        return ResponseEntity.ok(bookingRepository.getRentedBookings());
    }

    @Override
    public ResponseEntity<List<Booking>> getReservedBookings() {
        return ResponseEntity.ok(bookingRepository.getReservedBookings());
    }

    @Override
    public ResponseEntity<List<Booking>> getMyBookings(Long id) {
        return ResponseEntity.ok(bookingRepository.getMyBookings(id));
    }

    @Override
    public ResponseEntity<List<Booking>> getMyReservedBookings(Long id) {
        return ResponseEntity.ok(bookingRepository.getMyReservedBookings(id));
    }

    @Override
    public ResponseEntity<List<Booking>> getMyRentedBookings(Long id) {
        return ResponseEntity.ok(bookingRepository.getMyRentedBookings(id));
    }

    @Override
    public File createExcelBookingListExelFile(List<Booking> bookingList) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());

        String fileName = "bookings_" + timeStamp + ".xlsx";

        System.out.println(fileName);


        final File xls = new File(fileName);
        final FileOutputStream fos = new FileOutputStream(xls);

        Workbook workbook = new XSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet("Bookings");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        String[] rowNames = { "id", "userId", "vehicleId", "receiptDate", "returnDate", "bookingStateCode", "totalCost" };

        for (int i = 0; i < rowNames.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(rowNames[i]);
            cell.setCellStyle(headerCellStyle);
        }

        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy HH:mm:ss"));

        int rowNum = 1;
        for (Booking booking : bookingList) {
            Row row = sheet.createRow(rowNum++);

            row.createCell(0).setCellValue(booking.getId());
            row.createCell(1).setCellValue(booking.getUser().getId());
            row.createCell(2).setCellValue(booking.getVehicle().getId());

            Cell receiptDateCell = row.createCell(3);
            receiptDateCell.setCellValue(booking.getRentalDate().toString());
            receiptDateCell.setCellStyle(dateCellStyle);

            Cell returnDateCell = row.createCell(4);
            returnDateCell.setCellValue(booking.getReturnDate().toString());
            returnDateCell.setCellStyle(dateCellStyle);


            row.createCell(5).setCellValue(booking.getBookingStateCode());
            row.createCell(6).setCellValue(booking.getTotalCost().doubleValue());
        }

        for (int i = 0; i < rowNames.length; i++) {
            sheet.autoSizeColumn(i);
        }



        workbook.write(fos);
        fos.close();

        workbook.close();

        return xls;
    }

    @Override
    public void bookingCancel(Long bookingId) {
        Booking booking = bookingRepository.findById(bookingId).get();
        Vehicle vehicle = vehicleRepository.findById(booking.getVehicle().getId()).get();
        vehicle.setVehicleStatus("AVI");
        booking.setBookingStateCode("CAN");
        bookingRepository.save(booking);
        vehicleRepository.save(vehicle);

    }

    @Override
    public List<ChartResponse> getChartData() {
        return bookingRepository.getChartData();
    }

    @Override
    public void bookingRent(Long bookingId) {
        Booking booking = bookingRepository.findById(bookingId).get();
        booking.setBookingStateCode("REN");
        bookingRepository.save(booking);
    }

    @Override
    public void bookingReturn(Long bookingId) {
        Booking booking = bookingRepository.findById(bookingId).get();
        Vehicle vehicle = vehicleRepository.findById(booking.getVehicle().getId()).get();
        vehicle.setVehicleStatus("AVI");
        booking.setBookingStateCode("RET");
        bookingRepository.save(booking);
        vehicleRepository.save(vehicle);
    }
}
