package com.BnaBD.carrental.service.Equipment;

import com.BnaBD.carrental.domain.Equipment;

import java.util.List;

public interface EquipmentService {

    List<Equipment> getEquipment();

     void addEquipment(Equipment equipment);

     void deleteEquipmentById(String eqpCode);

    Equipment getEquipmentByCode(String equipmentCode);
}
