package com.BnaBD.carrental.service.Equipment;

import com.BnaBD.carrental.domain.Equipment;
import com.BnaBD.carrental.repository.EquipmentRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class EquipmentServiceImpl implements EquipmentService {

    private EquipmentRepository equipmentRepository;

    public EquipmentServiceImpl(EquipmentRepository equipmentRepository)
    {
        this.equipmentRepository = equipmentRepository;
    }

    @Override
    public List<Equipment> getEquipment() {
        return equipmentRepository.findAll();
    }

    @Override
    public void addEquipment(Equipment equipment) {
        equipmentRepository.save(equipment);
    }

    @Override
    public void deleteEquipmentById(String eqpCode) {
        Equipment equipment = getEquipmentByCode(eqpCode);
        equipmentRepository.delete(equipment);
    }

    @Override
    public Equipment getEquipmentByCode(String equipmentCode) {
       return equipmentRepository.getEquipmentByCode(equipmentCode);
    }
}
