package com.BnaBD.carrental.service.Registration;

import com.BnaBD.carrental.dto.request.SignupRequest;
import org.springframework.http.ResponseEntity;

public interface RegistrationService {
    ResponseEntity<?> saveUser(SignupRequest signUpRequest);
}
