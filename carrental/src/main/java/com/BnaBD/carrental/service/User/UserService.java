package com.BnaBD.carrental.service.User;

import com.BnaBD.carrental.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {
    Page<User> getUsersForPage(Pageable pageable);
    User getUserByID(Long id);
    ResponseEntity<?> updateUserData(String username, String email, User user);
}
