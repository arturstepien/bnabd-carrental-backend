package com.BnaBD.carrental.service.User;

import com.BnaBD.carrental.domain.Role;
import com.BnaBD.carrental.domain.RoleType;
import com.BnaBD.carrental.domain.User;
import com.BnaBD.carrental.dto.request.SignupRequest;
import com.BnaBD.carrental.dto.response.MessageResponse;
import com.BnaBD.carrental.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserServiceImpl implements  UserService {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder){
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Page<User> getUsersForPage(Pageable pageable) {
        return userRepository.getUsersForPage(pageable);
    }

    @Override
    public User getUserByID(Long id) {
        return userRepository.findById(id).get();
    }

    @Override
    @Transactional
    public ResponseEntity<?> updateUserData(String username, String email, User user) {
        User tempUser = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
        if (userRepository.existsByUsername(user.getUsername()) && !(username.equals(user.getUsername()))) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }
        else {
            tempUser.setUsername(user.getUsername());
            System.out.println(tempUser.getUsername()+" 1");
        }
        if (userRepository.existsByEmail(user.getEmail()) && !(email.equals(user.getEmail()))) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        } else {
            tempUser.setEmail(user.getEmail());
        }
        if (user.getPassword()!= null) {
            if (user.getPassword().length() < 6 || user.getPassword().length() > 40) {
                return ResponseEntity
                        .badRequest()
                        .body(new MessageResponse("The password must be between 6 and 40 characters."));
            } else {
                tempUser.setPassword(passwordEncoder.encode(user.getPassword()));
            }
        }
        System.out.println(tempUser.getUsername()+" 2");
        tempUser.setName(user.getName());
        tempUser.setSurname(user.getSurname());
        userRepository.save(tempUser);
        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));

    }

}

