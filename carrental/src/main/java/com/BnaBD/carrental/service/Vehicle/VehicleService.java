package com.BnaBD.carrental.service.Vehicle;

import com.BnaBD.carrental.domain.Vehicle;
import com.BnaBD.carrental.dto.request.VehicleFilterRequest;
import com.BnaBD.carrental.dto.request.VehicleRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import java.util.List;

public interface VehicleService {
    List<Vehicle> getVehicles();
    Page<Vehicle> getVehiclesForPage(Pageable pageable);
    Vehicle getVehicleByID(Long id);
    Vehicle getVehicleByID(String id);
    Vehicle updateVehicle(VehicleRequest vehicleRequest);
    Vehicle addVehicle(VehicleRequest vehicleRequest);
    List<String> getBrandList();
    List<String> getBodyTypeList();
    List<String> getColorList();
    List<String> getModelListForBrand(String brand);
    Page<Vehicle> getFilteredCarListForPage(VehicleFilterRequest vehicleFilter, Pageable pageable);

}
