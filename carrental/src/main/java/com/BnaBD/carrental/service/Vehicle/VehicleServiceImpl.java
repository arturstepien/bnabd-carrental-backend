package com.BnaBD.carrental.service.Vehicle;

import com.BnaBD.carrental.domain.Vehicle;
import com.BnaBD.carrental.domain.VehicleParameters;
import com.BnaBD.carrental.dto.request.VehicleFilterRequest;
import com.BnaBD.carrental.dto.request.VehicleRequest;
import com.BnaBD.carrental.dto.response.VehicleResponse;
import com.BnaBD.carrental.repository.VehicleParametersRepository;
import com.BnaBD.carrental.repository.VehicleRepository;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VehicleServiceImpl implements VehicleService{

    private VehicleRepository vehicleRepository;
    private VehicleParametersRepository vehicleParametersRepository;

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository, VehicleParametersRepository vehicleParametersRepository) {

        this.vehicleRepository = vehicleRepository;
        this.vehicleParametersRepository = vehicleParametersRepository;
    }
    @Override
    public List<Vehicle> getVehicles() {
        return vehicleRepository.findAll();
    }

    @Override
    public Page<Vehicle> getVehiclesForPage(Pageable pageable) {
        return vehicleRepository.getVehiclesForPage(pageable);
    }

    @Override
    public Vehicle getVehicleByID(Long id) {
        return vehicleRepository.findById(id).get();
    }

    @Override
    public Vehicle getVehicleByID(String id) {
        return vehicleRepository.findById(Long.parseLong(id)).get();
    }

    @Override
    public Vehicle updateVehicle(VehicleRequest vehicleRequest) {
        System.out.println("FName: "+vehicleRequest.getFileName());
        Vehicle vehicle = vehicleRepository.findById(vehicleRequest.getId()).get();
        vehicle.setBrand(vehicleRequest.getBrand());
        vehicle.setModel(vehicleRequest.getModel());
        vehicle.setDailyFee(vehicleRequest.getDailyFee());
        vehicle.setRegistration(vehicleRequest.getRegistration());
        vehicle.setVehicleStatus(vehicleRequest.getVehicleStatus());
        vehicle.setBestOffer(vehicleRequest.getBestOffer());

        vehicle.getVehicleParameters().setBodytype(vehicleRequest.getBodytype());
        vehicle.getVehicleParameters().setFuelType(vehicleRequest.getFuelType());
        vehicle.getVehicleParameters().setPower(vehicleRequest.getPower());
        vehicle.getVehicleParameters().setGearbox(vehicleRequest.getGearbox());
        vehicle.getVehicleParameters().setFrontWheelDrive(vehicleRequest.getFrontWheelDrive());
        vehicle.getVehicleParameters().setDoorsNumber(vehicleRequest.getDoorsNumber());
        vehicle.getVehicleParameters().setSeatsNumber(vehicleRequest.getSeatsNumber());
        vehicle.getVehicleParameters().setColor(vehicleRequest.getColor());
        vehicle.getVehicleParameters().setMetallic(vehicleRequest.getMetallic());
        vehicle.getVehicleParameters().setDescription(vehicleRequest.getDescription());
        vehicle.getVehicleParameters().setProductionYear(vehicleRequest.getProductionYear());
        vehicle.getVehicleParameters().setPhotoName(vehicleRequest.getFileName());

        vehicleRepository.save(vehicle);

        return vehicle;
    }

    @Override
    public Vehicle addVehicle(VehicleRequest vehicleRequest) {

        Vehicle vehicle = Vehicle.builder()
                .id((long) (vehicleRepository.findAll().size()+1))
                .registration(vehicleRequest.getRegistration())
                .brand(vehicleRequest.getBrand())
                .model(vehicleRequest.getModel())
                .dailyFee(vehicleRequest.getDailyFee())
                .vehicleStatus(vehicleRequest.getVehicleStatus())
                .bestOffer(vehicleRequest.getBestOffer())
                .build();


        vehicleRepository.save(vehicle);
        VehicleParameters vehicleParameters = VehicleParameters.builder()
                .vehicleID(vehicle.getId())
                .bodytype(vehicleRequest.getBodytype())
                .productionYear(vehicleRequest.getProductionYear())
                .fuelType(vehicleRequest.getFuelType())
                .power(vehicleRequest.getPower())
                .gearbox(vehicleRequest.getGearbox())
                .frontWheelDrive(vehicleRequest.getFrontWheelDrive())
                .doorsNumber(vehicleRequest.getDoorsNumber())
                .seatsNumber(vehicleRequest.getSeatsNumber())
                .color(vehicleRequest.getColor())
                .metallic(vehicleRequest.getMetallic())
                .photoName(vehicleRequest.getFileName())
                .description(vehicleRequest.getDescription())
                .build();


        vehicleParametersRepository.save(vehicleParameters);

        return vehicle;

    }

    @Override
    public List<String> getBrandList() {
        return vehicleRepository.getBrandList();
    }

    @Override
    public List<String> getBodyTypeList() {
        return vehicleRepository.getBodyTypeList();
    }

    @Override
    public List<String> getColorList() {
        return vehicleRepository.getColorList();
    }

    @Override
    public List<String> getModelListForBrand(String brand) {
        String formatedBrand = String.format("%s", brand.replace("\"", ""));
        formatedBrand = String.format("%s", formatedBrand.replace("=", ""));
        System.out.println(formatedBrand);
        return vehicleRepository.getModelListForBrand(formatedBrand);
    }

    @Override
    public Page<Vehicle> getFilteredCarListForPage(VehicleFilterRequest vehicleFilter, Pageable pageable) {

        return vehicleRepository.getFilteredCarListForPage(
                vehicleFilter.getBrand(),
                vehicleFilter.getModel(),
                vehicleFilter.getBodytype(),
                vehicleFilter.getPriceFrom(),
                vehicleFilter.getPriceTo(),
                vehicleFilter.getPlacesNumberFrom(),
                vehicleFilter.getPlacesNumberTo(),
                vehicleFilter.getDoorsNumberFrom(),
                vehicleFilter.getDoorsNumberTo(),
                vehicleFilter.getProductionYearFrom(),
                vehicleFilter.getProductionYearTo(),
                vehicleFilter.getColor(),
                pageable);
    }
}
