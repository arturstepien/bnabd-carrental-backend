
DROP TABLE IF EXISTS VehicleStatus CASCADE;
DROP TABLE IF EXISTS Vehicles CASCADE;
DROP TABLE IF EXISTS VehicleParameters CASCADE;
DROP TABLE IF EXISTS Equipment CASCADE;
DROP TABLE IF EXISTS Vehicle_Equipment CASCADE;
DROP TABLE IF EXISTS vehicleparameters CASCADE;
DROP TABLE IF EXISTS vehicle_parameters CASCADE;


INSERT INTO roles(id, name) VALUES (1, 'ROLE_ADMIN'), (2, 'ROLE_USER');
INSERT INTO users(id, email, name, password, surname, username) VALUES (1, 'admin@admin.com', 'admin','$2a$10$8wJpA5SsupCdSPjfdfDKG.VsRiOysrnq.M9bOBKN32NQwuY1bVap6', 'admin', 'admin');
INSERT INTO user_roles(user_id, role_id) VALUES (1, 2), (1, 1);



DROP TABLE IF EXISTS VehicleStatus CASCADE;
DROP TABLE IF EXISTS Vehicles CASCADE;
DROP TABLE IF EXISTS VehicleParameters CASCADE;
DROP TABLE IF EXISTS Equipment CASCADE;
DROP TABLE IF EXISTS Vehicle_Equipment CASCADE;
DROP TABLE IF EXISTS vehicleparameters CASCADE;
DROP TABLE IF EXISTS vehicle_parameters CASCADE;


CREATE TABLE Vehicle_Status(
  vehicle_Status_Code VARCHAR(3) NOT NULL,
  description VARCHAR(50) NOT NULL,
  PRIMARY KEY(vehicle_Status_Code)
);
CREATE TABLE Vehicles(
  ID SERIAL NOT NULL ,
  registration VARCHAR(20) NOT NULL,
  brand VARCHAR(50) NOT NULL,
  model VARCHAR(50) NOT NULL,
  daily_Fee decimal(15,2) NOT NULL,
  vehicle_status VARCHAR(3) NOT NULL,
  best_offer boolean NOT NULL,
  PRIMARY KEY(ID),
  FOREIGN KEY(vehicle_Status) REFERENCES Vehicle_Status(vehicle_Status_Code),
  UNIQUE (registration)
);
CREATE TABLE Vehicle_Parameters(
  vehicle_id SERIAL NOT NULL,
  bodytype VARCHAR(30) NOT NULL,
  production_Year INT  NOT NULL,
  fuel_Type VARCHAR(30) NOT NULL,
  power INT NOT NULL,
  gearbox VARCHAR(30) NOT NULL,
  front_Wheel_Drive boolean NOT NULL,
  doors_Number INT NOT NULL,
  seats_Number INT NOT NULL,
  color VARCHAR(30) NOT NULL,
  metallic boolean NOT NULL,
  photo_Name VARCHAR(70) NOT NULL,
  description VARCHAR(100) NOT NULL,
  FOREIGN KEY (vehicle_ID) REFERENCES Vehicles(ID),
  UNIQUE (vehicle_ID) 
);
CREATE TABLE Equipment(
  equipment_Code VARCHAR(3) NOT NULL,
  description VARCHAR(50) NOT NULL,
  PRIMARY KEY(equipment_Code)
);
CREATE TABLE Vehicle_Equipment(
  vehicle_ID SERIAL NOT NULL,
  equipment_ID VARCHAR(3) NOT NULL,
  PRIMARY KEY(vehicle_ID, equipment_ID),
  FOREIGN KEY(vehicle_ID) REFERENCES Vehicles(ID),
  FOREIGN KEY(equipment_ID) REFERENCES Equipment(equipment_Code)
);
CREATE TABLE BookingStateCodes(
  bookingCode VARCHAR(3) NOT NULL,
  description VARCHAR(50) NOT NULL,
  PRIMARY KEY(bookingCode)
);
CREATE TABLE Bookings(
  ID SERIAL NOT NULL,
  userID INT NOT NULL,
  vehicleID INT NOT NULL,
  rentalDate DATETIME NOT NULL,
  returnDate DATETIME NOT NULL,
  bookingStateCode VARCHAR(3) NOT NULL,
  totalCost decimal(15,2) NOT NULL,
  PRIMARY KEY(ID),
  FOREIGN KEY(userID) REFERENCES Users(ID),
  FOREIGN KEY(vehicleID) REFERENCES Vehicles(ID),
  FOREIGN KEY(bookingStateCode) REFERENCES BookingStateCodes(bookingCode)
);
DROP TABLE IF EXISTS BookingStateCodes CASCADE;
DROP TABLE IF EXISTS Bookings CASCADE;

CREATE TABLE BookingStateCodes(
  booking_code VARCHAR(3) NOT NULL,
  description VARCHAR(50) NOT NULL,
  PRIMARY KEY(booking_Code)
);
CREATE TABLE Bookings(
  ID SERIAL NOT NULL,
  user_ID INT NOT NULL,
  vehicle_ID INT NOT NULL,
  rental_Date timestamp NOT NULL,
  return_Date timestamp NOT NULL,
  booking_state_code VARCHAR(3) NOT NULL,
  total_Cost decimal(15,2) NOT NULL,
  PRIMARY KEY(ID),
  FOREIGN KEY(user_ID) REFERENCES Users(ID),
  FOREIGN KEY(vehicle_ID) REFERENCES Vehicles(ID),
  FOREIGN KEY(booking_State_Code) REFERENCES BookingStateCodes(booking_Code)
);


INSERT INTO Vehicle_Status VALUES ('AVI','available'),('UAV','unavailable');
INSERT INTO Vehicles VALUES (1,'AB 12321','Alfa Romeo','4C',183.04,'AVI',true),(2,'BC 57843','Audi','R8',124.95,'AVI',false),(3,'SD 85678','Audi','Q5',153.88,'AVI',true),(4,'QW 32456','Audi','S5',176.41,'AVI',false),(5,'RE 54343','Audi','A3',109.38,'AVI',true),(6,'WE 54545','Audi','80',193.67,'AVI',false),(7,'WY 65656','BMW','I8',200.80,'AVI',false),(8,'AB 74535','BMW','X1',144.43,'AVI',false),(9,'AS 75675','BMW','M5',165.06,'AVI',false),(10,'QW 34546','BMW','M6',169.25,'AVI',false),(11,'EW 75674','Citroen','C4 Cactus',163.55,'AVI',false),(12,'FD 34567','Citroen','Jumpy',192.15,'AVI',false),(13,'SD 64626','Citroen','Xsara Picasso',206.25,'AVI',false),(14,'FD 55678','Dacia','Duster',153.09,'AVI',false),(15,'QW 77654','Dacia','Logan',164.67,'AVI',false),(16,'EW 37658','Fiat','Panda',201.32,'AVI',false),(17,'FD 46544','Fiat','500',219.85,'AVI',false),(18,'SD 34435','Ford','Mustang',212.32,'AVI',false),(19,'AS 55454','Ford','Fiesta',226.59,'AVI',false),(20,'QW 34343','Ford','Crown Victoria',178.54,'AVI',true),(21,'EW 55445','Ford','Transit',219.45,'AVI',false),(22,'FD 44554','Ford','Escape',215.58,'AVI',false),(23,'AB 35434','Honda','Accord',187.30,'AVI',false),(24,'AS 24343','Honda','CR-V',245.97,'AVI',false),(25,'QW 62245','Hyundai','Genesis',208.25,'AVI',false),(26,'EW 5345','Hyundai','i30',129.88,'AVI',false),(27,'FD 65456','Hyundai','Santa Fe',230.71,'AVI',false),(28,'VA 75657','Jeep','Cherokee',200.59,'AVI',false),(29,'AB 45654','Jeep','Renegade',142.29,'AVI',false),(30,'DS 12321','Jeep','Wrangler',180.11,'AVI',false),(31,'VC 46567','Lexus','ES',203.13,'AVI',false),(32,'SD 12321','Lexus','IS',168.99,'AVI',true),(33,'AB 45656','Mercedes-Benz','AMG-GT',230.97,'AVI',false),(34,'WW 56756','Mercedes-Benz','G Class',149.63,'AVI',false),(35,'AB 164781','Mitsubishi','Outlander',196.20,'AVI',false),(36,'AD 35454','Mitsubishi','ASX',109.81,'AVI',true),(37,'AB 65645','Nissan','GTR',147.53,'AVI',false),(38,'AB 34545','Nissan','Juke',177.96,'AVI',false),(39,'AB 65445','Opel','Corsa',200.71,'AVI',false),(40,'DD 12321','Opel','Vectra',201.30,'AVI',false),(41,'AB 65656','Opel','Astra',180.94,'AVI',false),(42,'DS  12321','Porshe','911',180.62,'AVI',false),(43,'QW 64565','Porshe','Panamera',122.81,'AVI',false),(44,'RE  12321','Porshe','Cayenne',127.15,'AVI',false),(45,'WU 54545','Subaru','Impreza',123.65,'AVI',false),(46,'WI 12321','Volkswagen','Passat',243.33,'AVI',false),(47,'AB  34545','Volkswagen','Polo',213.35,'AVI',false),(48,'AS  12321','Volkswagen','Golf',122.42,'AVI',true),(49,'QW 64365','Volvo','V40',199.37,'AVI',false),(50,'EW 75675','Volvo','XC90',226.59,'AVI',false),(51,'ZZ 21243s','Ferrari','Enzo',255.22,'AVI',false),(52,'AS 2121as','Ferrari','California',225.22,'AVI',false);
INSERT INTO Vehicle_Parameters VALUES (1,'Coupe',2017,'LPG',240,'auto',false,2,2,'Red',true,'1.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(2,'Coupe',2014,'Petrol',500,'man',false,2,2,'White',true,'2.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(3,'SUV',2008,'LPG',240,'man',true,4,5,'Red',true,'3.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(4,'Sedan',2016,'Petrol',340,'auto',true,4,5,'Black',true,'4.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(5,'Kombi',2012,'Petrol',200,'man',false,3,5,'Blue',true,'5.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(6,'Kombi',2011,'LPG',100,'auto',true,5,5,'Black',true,'6.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(7,'Coupe',2014,'Petrol',231,'auto',false,2,2,'Grey',true,'7.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(8,'SUV',2016,'LPG',240,'man',true,4,5,'White',true,'8.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(9,'Coupe',2013,'Petrol',400,'auto',true,4,5,'Blue',true,'9.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(10,'Coupe',2014,'Petrol',560,'auto',true,4,5,'Black',true,'10.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(11,'SUV',2016,'Petrol',80,'auto',false,4,5,'White',true,'11.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(12,'Van',2013,'Petrol',115,'man',false,3,2,'Red',true,'12.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(13,'Minivan',2012,'LPG',105,'man',false,5,5,'Beige',false,'13.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus'),(14,'SUV',2017,'LPG',95,'man',true,2,2,'Black',true,'14.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(15,'Sedan',2015,'Petrol',80,'auto',true,2,2,'Brown',true,'15.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(16,'Hatchback',2017,'Petrol',85,'man',false,2,2,'Red',true,'16.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(17,'Hatchback',2017,'LPG',75,'auto',false,2,2,'White',true,'17.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(18,'Coupe',2016,'Petrol',350,'man',true,2,2,'Orange',true,'18.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(19,'Hatchback',2003,'LPG',100,'auto',true,2,5,'Grey',true,'19.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(20,'Sedan',2005,'Petrol',200,'auto',true,4,5,'White',false,'20.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(21,'Van',2017,'LPG',120,'man',true,5,2,'Grey',true,'21.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(22,'SUV',2013,'Petrol',150,'auto',true,5,5,'Blue',true,'22.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(23,'Coupe',2005,'Petrol',200,'man',false,5,4,'Claret',false,'23.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(24,'SUV',2006,'LPG',140,'auto',false,5,4,'Grey',true,'24.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(25,'Coupe',2011,'Petrol',315,'auto',false,4,4,'Black',true,'25.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(26,'Kombi',2013,'Petrol',100,'man',true,5,5,'Blue',true,'26.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(27,'SUV',2015,'LPG',160,'man',false,5,5,'Grey',true,'27.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(28,'SUV',2017,'Petrol',220,'man',false,5,5,'Black',true,'28.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(29,'SUV',2016,'Petrol',130,'man',true,5,5,'Black',true,'29.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(30,'SUV',2017,'Petrol',200,'man',false,5,5,'Beige',true,'30.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(31,'Sedan',2013,'Petrol',272,'auto',false,5,5,'Red',true,'31.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(32,'Sedan',2017,'LPG',290,'man',true,4,5,'Red',true,'32.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(33,'Coupe',2015,'LPG',500,'auto',true,2,2,'Grey',true,'33.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(34,'Sedan',2016,'Petrol',200,'man',false,5,5,'Black',true,'34.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(35,'SUV',2017,'LPG',145,'auto',false,5,5,'Claret',true,'35.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(36,'SUV',2018,'Petrol',120,'man',true,5,5,'Red',true,'36.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(37,'Coupe',2017,'Petrol',500,'auto',false,2,2,'White',true,'37.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(38,'SUV',2018,'LPG',120,'man',true,3,5,'Black',true,'38.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(39,'Hatchback',2015,'Petrol',90,'man',false,5,5,'Red',false,'39.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(40,'Sedan',2014,'LPG',150,'auto',true,5,5,'Grey',false,'40.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(41,'Kombi',2015,'LPG',130,'man',false,5,5,'Red',false,'41.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(42,'Coupe',2014,'Petrol',411,'auto',false,2,2,'White',true,'42.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(43,'Coupe',2013,'Petrol',300,'auto',true,4,5,'Black',true,'43.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(44,'SUV',2012,'Petrol',290,'auto',true,5,5,'Black',true,'44.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(45,'Coupe',2011,'Petrol',250,'auto',true,5,5,'Blue',true,'45.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(46,'Sedan',2014,'Petrol',150,'auto',false,5,5,'White',true,'46.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(47,'Hatchback',2015,'LPG',90,'man',false,5,5,'White',true,'47.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(48,'Hatchback',2016,'Petrol',125,'man',true,5,5,'Black',true,'48.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(49,'SUV',2017,'Petrol',160,'auto',false,5,5,'Brown',true,'49.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(50,'SUV',2018,'LPG',224,'man',false,5,5,'Black',true,'50.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus '),(51,'Coupe',2015,'Petrol',660,'man',false,2,2,'Red',true,'d0629f27-f3c5-48ec-b75a-1527a4eac728.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus'),(52,'Coupe',2015,'Petrol',460,'man',false,2,2,'Red',true,'2cdedbd0-323f-4f0b-b999-2cbf4ed0c662.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget finibus turpis, in rhoncus');
INSERT INTO Equipment VALUES ('ABS','ABS'),('AF','Alufelgi'),('APS','Podgrzewane przednie siedzenia'),('CB','CB Radio'),('CD','CD'),('CZ','Centralny zamek'),('ESP','Elektryczne przednie szyby'),('EUL','Elektrycznie ustawiane lusterka'),('IM','Immobilizer'),('KM','Klimatyzacja manualna'),('PPK','Poduszka powietrzna kierowcy'),('RD','Radio'),('SP','Światła przeciwmgielne'),('WK','Wspomaganie kierownicy');
INSERT INTO Vehicle_Equipment VALUES (1,'AF'),(1,'CB'),(1,'CD'),(1,'CZ'),(1,'ESP'),(1,'EUL'),(1,'PPK'),(1,'SP'),(1,'WK'),(2,'ABS'),(2,'APS'),(2,'CB'),(2,'CD'),(2,'CZ'),(2,'ESP'),(2,'EUL'),(2,'KM'),(2,'RD'),(3,'ABS'),(3,'AF'),(3,'APS'),(3,'CB'),(3,'CD'),(3,'KM'),(3,'PPK'),(3,'RD'),(3,'SP'),(4,'AF'),(4,'CB'),(4,'CD'),(4,'ESP'),(4,'KM'),(4,'PPK'),(4,'RD'),(4,'SP'),(4,'WK'),(5,'ABS'),(5,'AF'),(5,'CB'),(5,'CD'),(5,'CZ'),(5,'ESP'),(5,'EUL'),(5,'KM'),(5,'RD'),(5,'SP'),(5,'WK'),(6,'ABS'),(6,'AF'),(6,'CB'),(6,'CZ'),(6,'ESP'),(6,'EUL'),(6,'PPK'),(6,'WK'),(7,'APS'),(7,'CB'),(7,'CD'),(7,'CZ'),(7,'KM'),(7,'PPK'),(7,'RD'),(7,'SP'),(7,'WK'),(8,'ABS'),(8,'CB'),(8,'CD'),(8,'EUL'),(8,'KM'),(8,'PPK'),(8,'RD'),(8,'SP'),(8,'WK'),(9,'AF'),(9,'APS'),(9,'CB'),(9,'ESP'),(9,'EUL'),(9,'KM'),(9,'SP'),(9,'WK'),(10,'AF'),(10,'APS'),(10,'CB'),(10,'CD'),(10,'ESP'),(10,'EUL'),(10,'SP'),(10,'WK'),(11,'ABS'),(11,'AF'),(11,'CB'),(11,'CD'),(11,'CZ'),(11,'ESP'),(11,'EUL'),(11,'KM'),(11,'PPK'),(11,'WK'),(12,'ABS'),(12,'AF'),(12,'APS'),(12,'CZ'),(12,'ESP'),(12,'EUL'),(12,'KM'),(12,'WK'),(13,'ABS'),(13,'AF'),(13,'CB'),(13,'CD'),(13,'CZ'),(13,'EUL'),(13,'KM'),(13,'PPK'),(13,'SP'),(13,'WK'),(14,'AF'),(14,'CB'),(14,'CD'),(14,'CZ'),(14,'ESP'),(14,'EUL'),(14,'KM'),(14,'RD'),(14,'WK'),(15,'AF'),(15,'CB'),(15,'CD'),(15,'ESP'),(15,'EUL'),(15,'KM'),(15,'RD'),(15,'WK'),(16,'ABS'),(16,'APS'),(16,'CB'),(16,'CD'),(16,'CZ'),(16,'ESP'),(16,'EUL'),(16,'PPK'),(17,'ABS'),(17,'AF'),(17,'APS'),(17,'CB'),(17,'CD'),(17,'KM'),(17,'PPK'),(17,'RD'),(17,'SP'),(17,'WK'),(18,'ABS'),(18,'APS'),(18,'CD'),(18,'KM'),(18,'PPK'),(18,'RD'),(18,'SP'),(18,'WK'),(19,'APS'),(19,'CD'),(19,'CZ'),(19,'EUL'),(19,'KM'),(19,'PPK'),(19,'RD'),(19,'SP'),(19,'WK'),(20,'ABS'),(20,'APS'),(20,'CB'),(20,'CD'),(20,'CZ'),(20,'ESP'),(20,'KM'),(20,'RD'),(20,'SP'),(20,'WK'),(21,'APS'),(21,'CB'),(21,'CD'),(21,'CZ'),(21,'ESP'),(21,'EUL'),(21,'RD'),(21,'SP'),(22,'APS'),(22,'CB'),(22,'CD'),(22,'EUL'),(22,'KM'),(22,'PPK'),(22,'RD'),(22,'SP'),(22,'WK'),(23,'ABS'),(23,'AF'),(23,'APS'),(23,'CB'),(23,'CD'),(23,'CZ'),(23,'KM'),(23,'PPK'),(23,'RD'),(24,'APS'),(24,'CB'),(24,'CD'),(24,'CZ'),(24,'ESP'),(24,'EUL'),(24,'RD'),(24,'SP'),(24,'WK'),(25,'ABS'),(25,'APS'),(25,'CB'),(25,'CD'),(25,'KM'),(25,'RD'),(25,'SP'),(25,'WK'),(26,'AF'),(26,'CB'),(26,'CD'),(26,'CZ'),(26,'ESP'),(26,'EUL'),(26,'PPK'),(26,'SP'),(26,'WK'),(27,'APS'),(27,'CB'),(27,'CD'),(27,'ESP'),(27,'EUL'),(27,'KM'),(27,'SP'),(27,'WK'),(28,'AF'),(28,'CB'),(28,'CD'),(28,'ESP'),(28,'EUL'),(28,'KM'),(28,'PPK'),(28,'WK'),(29,'ABS'),(29,'AF'),(29,'APS'),(29,'CB'),(29,'ESP'),(29,'EUL'),(29,'PPK'),(29,'SP'),(30,'ABS'),(30,'AF'),(30,'CB'),(30,'CD'),(30,'ESP'),(30,'KM'),(30,'RD'),(30,'SP'),(31,'ABS'),(31,'APS'),(31,'CD'),(31,'ESP'),(31,'EUL'),(31,'KM'),(31,'PPK'),(31,'SP'),(32,'ABS'),(32,'AF'),(32,'CD'),(32,'ESP'),(32,'EUL'),(32,'PPK'),(32,'RD'),(32,'SP'),(32,'WK'),(33,'ABS'),(33,'AF'),(33,'CB'),(33,'CD'),(33,'ESP'),(33,'PPK'),(33,'RD'),(33,'SP'),(33,'WK'),(34,'ABS'),(34,'AF'),(34,'CB'),(34,'EUL'),(34,'KM'),(34,'PPK'),(34,'RD'),(34,'SP'),(35,'ABS'),(35,'AF'),(35,'CB'),(35,'CD'),(35,'CZ'),(35,'ESP'),(35,'EUL'),(35,'PPK'),(35,'SP'),(36,'AF'),(36,'CZ'),(36,'ESP'),(36,'EUL'),(36,'KM'),(36,'PPK'),(36,'RD'),(36,'SP'),(36,'WK'),(37,'ABS'),(37,'AF'),(37,'APS'),(37,'CB'),(37,'CZ'),(37,'ESP'),(37,'RD'),(37,'SP'),(37,'WK'),(38,'AF'),(38,'CB'),(38,'CD'),(38,'CZ'),(38,'ESP'),(38,'EUL'),(38,'PPK'),(38,'RD'),(38,'SP'),(39,'ABS'),(39,'AF'),(39,'APS'),(39,'CB'),(39,'CD'),(39,'CZ'),(39,'KM'),(39,'PPK'),(39,'SP'),(39,'WK'),(40,'ABS'),(40,'AF'),(40,'CB'),(40,'CZ'),(40,'ESP'),(40,'PPK'),(40,'RD'),(40,'WK'),(41,'ABS'),(41,'AF'),(41,'APS'),(41,'CB'),(41,'CD'),(41,'ESP'),(41,'EUL'),(41,'KM'),(41,'PPK'),(42,'ABS'),(42,'AF'),(42,'APS'),(42,'CB'),(42,'CZ'),(42,'ESP'),(42,'EUL'),(42,'PPK'),(42,'SP'),(43,'ABS'),(43,'AF'),(43,'APS'),(43,'CB'),(43,'CD'),(43,'ESP'),(43,'KM'),(43,'PPK'),(43,'RD'),(44,'APS'),(44,'CB'),(44,'CD'),(44,'CZ'),(44,'ESP'),(44,'EUL'),(44,'PPK'),(44,'SP'),(44,'WK'),(45,'AF'),(45,'CB'),(45,'CD'),(45,'ESP'),(45,'EUL'),(45,'KM'),(45,'PPK'),(45,'RD'),(45,'SP'),(45,'WK'),(46,'AF'),(46,'APS'),(46,'CB'),(46,'CD'),(46,'CZ'),(46,'ESP'),(46,'KM'),(46,'SP'),(47,'ABS'),(47,'AF'),(47,'APS'),(47,'CB'),(47,'CZ'),(47,'ESP'),(47,'EUL'),(47,'KM'),(47,'PPK'),(47,'WK'),(48,'ABS'),(48,'AF'),(48,'CZ'),(48,'ESP'),(48,'EUL'),(48,'KM'),(48,'PPK'),(48,'SP'),(48,'WK'),(49,'ABS'),(49,'AF'),(49,'CB'),(49,'CD'),(49,'CZ'),(49,'ESP'),(49,'KM'),(49,'PPK'),(49,'RD'),(49,'SP'),(50,'AF'),(50,'APS'),(50,'CB'),(50,'ESP'),(50,'EUL'),(50,'IM'),(50,'KM'),(50,'RD'),(50,'WK'),(51,'ABS'),(51,'APS'),(51,'CB'),(51,'CD'),(51,'CZ'),(51,'ESP'),(51,'EUL'),(51,'IM'),(51,'KM'),(51,'PPK'),(51,'RD'),(51,'SP'),(51,'WK'),(52,'ABS'),(52,'AF'),(52,'APS'),(52,'CB'),(52,'CZ'),(52,'ESP'),(52,'EUL'),(52,'IM'),(52,'KM'),(52,'PPK'),(52,'RD'),(52,'WK');
INSERT INTO BookingStateCodes VALUES ('CAN','canceled'),('REN','rented'),('RES','reserved'),('RET','returned');